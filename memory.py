import numpy as np
from RNNplus import RNNplus

def save(model):
	np.savez('emily',
		E=model.E.get_value(),
		W_ih=model.W_ih.get_value(),
		W_hh=model.W_hh.get_value(),
		W_ii=model.W_ii.get_value(),
		W_hi=model.W_hi.get_value(),
		b_i=model.b_i.get_value(),
		b_h=model.b_h.get_value(),
		hidden=model.hidden_dim, input=model.input_dim)

def load(data, hidden_reduce):
	npzfile = np.load('emily.npz')

	E, W_ih, W_hh, W_ii, W_hi, b_i, b_h = npzfile["E"], npzfile["W_ih"], npzfile["W_hh"], npzfile["W_ii"], npzfile["W_hi"], npzfile["b_i"], npzfile["b_h"]
	input_dim = npzfile["input"]
	hidden_dim = npzfile["hidden"]
	hidden_dim = hidden_dim - hidden_reduce
	print ("====loading...=======")
	model = RNNplus(data, input_dim, hidden_dim)
	model.E.set_value(E)
	model.W_ih.set_value(W_ih[:,:,:hidden_dim])
	model.W_hh.set_value(W_hh[:,:hidden_dim, :hidden_dim])
	model.W_ii.set_value(W_ii)
	model.W_hi.set_value(W_hi[:,:hidden_dim,:])
	model.b_i.set_value(b_i)
	model.b_h.set_value(b_h[:,:hidden_dim])
	print("Model with {0} inputs and {1} hidden loaded!".format(input_dim, hidden_dim))
	return model
