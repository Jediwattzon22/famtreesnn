from RNNplus import RNNplus
from cacheschema import write_out
import numpy as np
import operator

def sgd_callback(model, X, num_examples_seen):
	loss = model.calculate_loss(X[-1000:])
	write_out(str("======%f=====\n" % loss))
	print("======%f=====" % loss)
	if loss < 7:
		data = model.cache_obj
		x_d = model.input_dim + 88
		if model.cache_obj.more_dictionary(x_d):
			print("number of words: {0}".format(x_d))
			model.dynamic_parameters(x_d)
		else:
			print("append more entries here")
		
def train_with_sgd(model, X, learning_rate=0.001, nepoch=20, decay=0.9, callback_every=5, callback=None):
	num_examples_seen = 0
	for epoch in range(nepoch):
		for i in np.random.permutation(len(X)):
			model.sgd_step(X[i], learning_rate, decay)
			num_examples_seen += 1
			if (num_examples_seen % callback_every) == 0:
				callback(model, X, num_examples_seen)
				break

def gradient_check_theano(model, x, h=0.001, error_threshold=0.01):
		# Overwrite the bptt attribute. We need to backpropagate all the way to get the correct gradient
		model.bptt_truncate = 1000
		# Calculate the gradients using backprop
		bptt_gradients = model.bptt([x])
		# List of all parameters we want to chec.
		model_parameters = ['E', 'U', 'W', 'b', 'V', 'c']
		# Gradient check for each parameter
		for pidx, pname in enumerate(model_parameters):
			# Get the actual parameter value from the mode, e.g. model.W
			parameter_T = operator.attrgetter(pname)(model)
			parameter = parameter_T.get_value()
			print ("Performing gradient check for parameter %s with size %d." % (pname, np.prod(parameter.shape)))
			# Iterate over each element of the parameter matrix, e.g. (0,0), (0,1), ...
			it = np.nditer(parameter, flags=['multi_index'], op_flags=['readwrite'])
			while not it.finished:
				ix = it.multi_index
				# Save the original value so we can reset it later
				original_value = parameter[ix]
				# Estimate the gradient using (f(x+h) - f(x-h))/(2*h)
				parameter[ix] = original_value + h
				parameter_T.set_value(parameter)
				gradplus = model.calculate_total_loss([x])
				parameter[ix] = original_value - h
				parameter_T.set_value(parameter)
				gradminus = model.calculate_total_loss([x])
				estimated_gradient = (gradplus - gradminus)/(2*h)
				parameter[ix] = original_value
				parameter_T.set_value(parameter)
				# The gradient for this parameter calculated using backpropagation
				backprop_gradient = bptt_gradients[pidx][ix]
				# calculate The relative error: (|x - y|/(|x| + |y|))
				relative_error = np.abs(backprop_gradient - estimated_gradient)/(np.abs(backprop_gradient) + np.abs(estimated_gradient))
				# If the error is to large fail the gradient check
				if relative_error > error_threshold:
					print ("Gradient Check ERROR: parameter=%s ix=%s" % (pname, ix))
					print ("+h Loss: %f" % gradplus)
					print ("-h Loss: %f" % gradminus)
					print ("Estimated_gradient: %f" % estimated_gradient)
					print ("Backpropagation gradient: %f" % backprop_gradient)
					print ("Relative Error: %f" % relative_error)
					return 
				it.iternext()
			print ("Gradient check for parameter %s passed." % (pname))
