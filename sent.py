import numpy as np
from memory import load
from cacheschema import DataCache

def generate_prediction_list():
	predictions = [0]
	while not predictions[-1] == 20:
		next_prediction_probs = model.predict(predictions)[-1]
		samples = np.random.multinomial(1, next_prediction_probs)
		sampled_prediction = np.argmax(samples)
		predictions.append(sampled_prediction)
		if len(predictions) > 200:
			print ("over!!! 123456789456123456789456123")
			break
	return predictions

if __name__ == "__main__":
	data = DataCache()
	data.build_data()
	model = load(data, 0)
	model.cache_obj.part_reverse_dictionary(model.input_dim)
	for x in range(10):
		print("\n")
		model.cache_obj.decode(generate_prediction_list())
