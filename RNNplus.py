import numpy as np
import theano as theano
import theano.tensor as T
from theano.gradient import grad_clip
import time
import operator

class RNNplus:
	
	def __init__(self, cache, input_dim, hidden_dim=32, layers=3, bptt_truncate=10):
		# Assign instance variables
		self.cache_obj = cache
		self.input_dim = input_dim
		self.hidden_dim = hidden_dim
		self.bptt_truncate = bptt_truncate
		self.layers =layers
		# Initialize the network parameters
		E = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (input_dim, input_dim))
		W_ih = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./hidden_dim), (layers*2, input_dim, hidden_dim))
		W_hh = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (layers*2, hidden_dim, hidden_dim))
		W_ii = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (layers*2, input_dim, input_dim))
		W_hi = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (layers*2, hidden_dim, input_dim))
		b_i = np.zeros((layers*2, input_dim))
		b_h = np.zeros((layers*2, hidden_dim))

		# Theano: Created shared variables
		self.E = theano.shared(name='E', value=E.astype(theano.config.floatX))
		self.W_ih = theano.shared(name='W_ih', value=W_ih.astype(theano.config.floatX))
		self.W_hh = theano.shared(name='W_hh', value=W_hh.astype(theano.config.floatX))
		self.W_ii = theano.shared(name='W_ii', value=W_ii.astype(theano.config.floatX))
		self.W_hi = theano.shared(name='W_hi', value=W_hi.astype(theano.config.floatX))
		self.b_i = theano.shared(name='b_i', value=b_i.astype(theano.config.floatX))
		self.b_h = theano.shared(name='b_h', value=b_h.astype(theano.config.floatX))

		# SGD / rmsprop: Initialize parameters
		self.mE = theano.shared(name='mE', value=np.zeros(E.shape).astype(theano.config.floatX))
		self.mW_ih = theano.shared(name='mW_ih', value=np.zeros(W_ih.shape).astype(theano.config.floatX))
		self.mW_hh = theano.shared(name='mW_hh', value=np.zeros(W_hh.shape).astype(theano.config.floatX))
		self.mW_ii = theano.shared(name='mW_ii', value=np.zeros(W_ii.shape).astype(theano.config.floatX))
		self.mW_hi = theano.shared(name='mW_hi', value=np.zeros(W_hi.shape).astype(theano.config.floatX))
		self.mb_i = theano.shared(name='mb_i', value=np.zeros(b_i.shape).astype(theano.config.floatX))
		self.mb_h = theano.shared(name='mb_h', value=np.zeros(b_h.shape).astype(theano.config.floatX))

		# We store the Theano graph here
		self.theano = {}
		self.__theano_build__()
	
	def __theano_build__(self):
		E, W_ih, W_hh, W_ii, W_hi, b_i, b_h = self.E, self.W_ih, self.W_hh, self.W_ii, self.W_hi, self.b_i, self.b_h
		
		x = T.ivector('x')
		
		def forward_prop_step(x_t, h_prev):		
			# Word embedding layer
			x_e = E[:,x_t]			
			h = h_prev

			for l in range(self.layers):
				y_in = T.tanh( W_ih[l].dot(h) + W_ii[l].dot(x_e) + b_i[l])
				y_g = T.nnet.hard_sigmoid(W_ih[l+1].dot(h) + W_ii[l+1].dot(x_e) + b_i[l+1])
				x = (T.ones_like(y_g) - y_g) * y_in + y_g * x_e

				h_in = T.nnet.relu(W_hh[l].dot(h) + W_hi[l].dot(x_e) + b_h[l])
				h_g = T.nnet.hard_sigmoid(W_hh[l+1].dot(h) +  W_hi[l+1].dot(x_e) + b_h[l+1])
				h = (T.ones_like(h_g) - h_g) * h_in + h_g * h
				x_e = x

			# Final output calculation
			# Theano's softmax returns a matrix with one row, we only need the row
			o_t = T.nnet.softmax(x_e)[0]

			return [o_t, h]
		
		[o, s], updates = theano.scan(
			forward_prop_step,
			sequences=x,
			truncate_gradient=self.bptt_truncate,
			outputs_info=[None, 
						  dict(initial=T.zeros(self.hidden_dim))])
		
		prediction = T.argmax(o, axis=1)
		o_error = T.sum(T.nnet.categorical_crossentropy(o, x))
		
		# Total cost (could add regularization here)
		cost = o_error
		
		# Gradients
		dE = T.grad(cost, E)
		dW_ih = T.grad(cost, W_ih)
		dW_hh = T.grad(cost, W_hh)
		dW_ii = T.grad(cost, W_ii)
		dW_hi = T.grad(cost, W_hi)
		db_i = T.grad(cost, b_i)
		db_h = T.grad(cost, b_h)

		# Assign functions
		self.predict = theano.function([x], o)
		self.predict_class = theano.function([x], prediction)
		self.ce_error = theano.function([x], cost)
		self.bptt = theano.function([x], [dE, dW_ih, dW_hh, dW_ii, dW_hi, db_i, db_h])
		
		# SGD parameters
		learning_rate = T.scalar('learning_rate')
		decay = T.scalar('decay')
		
		# rmsprop cache updates
		mE = decay * self.mE + (1 - decay) * dE ** 2
		mW_ih = decay * self.mW_ih + (1 - decay) * dW_ih ** 2
		mW_hh = decay * self.mW_hh + (1 - decay) * dW_hh ** 2
		mW_ii = decay * self.mW_ii + (1 - decay) * dW_ii ** 2
		mW_hi = decay * self.mW_hi + (1 - decay) * dW_hi ** 2
		mb_i = decay * self.mb_i + (1 - decay) * db_i ** 2
		mb_h = decay * self.mb_h + (1 - decay) * db_h ** 2
		
		self.sgd_step = theano.function(
			[x, learning_rate, theano.Param(decay, default=0.9)],
			[], 
			updates=[(E, E - learning_rate * dE / T.sqrt(mE + 1e-6)),
					 (W_ih, W_ih - learning_rate * dW_ih / T.sqrt(mW_ih + 1e-6)),
					 (W_hh, W_hh - learning_rate * dW_hh / T.sqrt(mW_hh + 1e-6)),
					 (W_ii, W_ii - learning_rate * dW_ii / T.sqrt(mW_ii + 1e-6)),
					 (W_hi, W_hi - learning_rate * dW_hi / T.sqrt(mW_hi + 1e-6)),
					 (b_i, b_i - learning_rate * db_i / T.sqrt(mb_i + 1e-6)),
					 (b_h, b_h - learning_rate * db_h / T.sqrt(mb_h + 1e-6)),
					 (self.mE, mE),
					 (self.mW_ih, mW_ih),
					 (self.mW_hh, mW_hh),
					 (self.mW_ii, mW_ii),
					 (self.mW_hi, mW_hi),
					 (self.mb_i, mb_i),
					 (self.mb_h, mb_h)
					])
		
	def dynamic_parameters(self, input_delta, hidden_delta=0):
		hidden_dim = self.b_h.get_value().shape[1]
		input_dim = self.input_dim
		layers = self.layers
		if input_delta > input_dim:
			E = np.random.uniform(-np.sqrt(1./input_delta), np.sqrt(1./input_delta), (input_dim, (input_delta - input_dim)))
			E2 = np.random.uniform(-np.sqrt(1./input_delta), np.sqrt(1./input_delta), ((input_delta - input_dim), input_delta))

			W_ih = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./hidden_dim), (layers*2, input_delta - input_dim, hidden_dim))
			
			W_ii = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (layers*2, input_delta - input_dim, input_dim))
			W_ii2 = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (layers*2, input_delta, input_delta - input_dim))

			W_hi = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (layers*2, hidden_dim, input_delta - input_dim))      
			b_i = np.zeros((layers*2, input_delta - input_dim))

			mE = np.zeros((input_dim, (input_delta-input_dim)))
			mE2 = np.zeros(((input_delta-input_dim), input_delta))

			mW_ih = np.zeros((layers*2, input_delta - input_dim, hidden_dim))

			mW_ii = np.zeros((layers*2, input_delta - input_dim, input_dim))
			mW_ii2 = np.zeros((layers*2, input_delta, input_delta - input_dim))

			mW_hi = np.zeros((layers*2, hidden_dim, input_delta - input_dim))      
			mb_i = np.zeros((layers*2, input_delta - input_dim))

			Matrices = [self.E, self.E, self.W_ih, self.W_ii, self.W_ii, self.W_hi, self.b_i, self.mE, self.mE, self.mW_ih, self.mW_ii, self.mW_ii, self.mW_hi, self.mb_i]
			Updates = [E, E2, W_ih, W_ii,  W_ii2, W_hi, b_i, mE, mE2, mW_ih, mW_ii, mW_ii2, mW_hi, mb_i]
			axis = [1, 0, 1, 1, 2, 2, 1, 1, 0, 1, 1, 2, 2, 1] 
			for i, M in enumerate(Matrices):
				M.set_value(np.concatenate([M.get_value(), Updates[i]], axis=axis[i]), borrow=True)
			self.input_dim = input_delta



	def calculate_total_loss(self, X):
		return np.sum([self.ce_error(x) for x in X])
	
	def calculate_loss(self, X):
		# Divide calculate_loss by the number of words
		num_words = np.sum([len(x) for x in X])
		return self.calculate_total_loss(X)/float(num_words)
