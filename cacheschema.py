from bs4 import BeautifulSoup
from urllib.request import urlopen
import nltk
import json

def write_out(s):
	with open("output", "a") as o:
		o.write(s)
		o.close()

class DataCache:

	def __init__(self):
		self.dictionary = {}
		self.rdictionary = {}
		self.batch = []
		self.textfile = open("urllist.txt", 'a')
		self.textlist = self.textfile
		self.words = 0

	def build_data(self):
		try:
			self.batch = self.load('batch')
			self.dictionary = self.load('dictionary')
		except:
			self.scrape()
			self.save(self.batch, 'batch')
			self.save(self.dictionary, 'dictionary')

	def append_data(self, applist):
		if self.batch and self.dictionary:
			self.textlist = applist
			self.scrape()
			for entry in applist:
				self.textfile.write(entry)
			self.save(self.batch, 'batch')
			self.save(self.dictionary, 'dictionary')

	def scrape(self):
		sd = nltk.data.load('tokenizers/punkt/english.pickle')
		for url in self.textlist:
			text = BeautifulSoup(urlopen(url), 'lxml')
			for script in text(["script", "style"]):
				script.extract()
			for sentence in sd.tokenize(text.get_text()):
				tokes = ["BEGIN"] + nltk.word_tokenize(sentence) + ["END"]
				[self.dictionary.update({word: len(self.dictionary)}) for word in nltk.FreqDist(tokes) if word not in self.dictionary]
				self.batch.append([self.dictionary[word] for word in tokes])

	def part_batch(self, i):
		inter_batch = []
		for data in self.batch:
			if max(data) < i:
				inter_batch.append(data)
			else:
				break
		return inter_batch

	def part_dictionary(self, i):
		self.dictionary = {key: value for value, key in enumerate(self.dictionary) if value < i}

	def part_reverse_dictionary(self, i):
		self.rdictionary = {value: key for value, key in enumerate(self.dictionary) if value < i}

	def more_dictionary(self, i):
		return max(self.dictionary.values(), key=int) > i

	def decode(self, batch):
		temp = []
		for b in batch:
			temp.append(self.rdictionary[b])
		print(' '.join(temp))

	def save(self, data, file):
		with open(file, 'w') as outfile:
			json.dump(data, outfile)

	def load(self, file):
		with open(file) as infile:
			return json.load(infile)
