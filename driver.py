from cacheschema import DataCache
from RNNplus import RNNplus
from train import train_with_sgd, sgd_callback
from memory import save, load
import os

os.environ["OMP_NUM_THREADS"] = "22"

print("===generating data===")
data = DataCache()
data.build_data()

print ('===building model====')
x_d = 220
if os.path.exists('emily.npz'):
	model = load(data, 7)
	x_d = model.input_dim
	x = data.part_batch(x_d)

else:
	model = RNNplus(cache=data, input_dim=x_d)
	x = data.part_batch(x_d)

print ('====model built=====')

while True:
	train_with_sgd(model, x, learning_rate=0.001, nepoch=6, decay=0.9, callback_every=5, callback=sgd_callback)
	x = data.part_batch(model.input_dim)
	save(model)
